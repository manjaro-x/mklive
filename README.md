## Mengapa Memilih Manjaro-X?

**[Manjaro-X](https://manjaro-x.id/)** merupakan pemasang Manjaro Linux yang lengkap untuk orang awam, desainer, pengajar, animator, dan pengembang aplikasi GTK & QT. Setelah memasang Manjaro-X versi rilis sistem operasi akan kembali menggunakan Manjaro linux. Sebab Manjaro-X bukanlah distribusi dengan branding setelah dipasang, melainkan sistem operasi Manjaro Linux yang di bundel kembali untuk kepraktisan penggunaan.

Tujuan Manjaro-X ini hanyalah untuk kepentingan pendidikan GNU/Linux semata, agar tercapainya kemudahan dan kenyamanan bagi pengguna yang pertama kali melihat sistem operasi GNU/Linux. Tujuan lainnya Manjaro-X ini untuk memperkaya jenis distribusi Manjaro Linux tek resmi sebagai sistem operasi GNU/Linux yang sederhana, kuat, dan elegan.

Di Manjaro-X beberapa tema dan gambar dinding khas Manjaro dihilangkan sehingga tampilan Manjaro-X lebih bersih dan natural. Dengan demikian akan mengembalikan _cita rasa_ Lingkungan Destop (DE) itu sendiri.

# Unduh

Manjaro-X hanya tersedia dengan arsitektur 64 bit. Klik [Unduh Manjaro-X](https://manjaro-x.id/).

***
# Cara Membangun Manjaro-X

```
# sudo ./mklive help
 script     : builder Manjaro-X GNU/Linux
 website    : https://manjaro-x.id
 license    : GPLv3
 usage 1    : ./$NAME [options]
 usage 2    : ./$NAME [action] [variant]
 options    : install           install depedency
              status            status cache and iso
              help              display this message
              version           display $NAME version
 action     : chroot            build new chroot directory
              iso               build iso from chroot directory
              full              chroot and build iso, without cleaning old chroot
              lazy              clean, chroot, build iso together
              clean             clean old chroot files. backup your iso first!
              cleanall          clean old all variant chroot files. are you sure?
 variant    : gnome             build gnome
              kde               build kde
              kde-developer     build kde for developer
              kde-design-suite  build kde for design suite
              kde-education     build kde for education
              kde-cad           build kde for cad
              kde-finance       build kde for finance
              kde-melody        build kde for musician
```

# Hasil kompil chroot

  `/var/lib/manjaro-tools/buildiso/`

#  Hasil kompil iso

  `/var/cache/manjaro-tools/iso/manjaro-x`

***
# Contact

* Email : [os.manjaro.x@gmail.com](os.manjaro.x@gmail.com)
* Telegram channel :[t.me/manjaro_x](t.me/manjaro_x)
