## Developer

## Network
avahi
networkmanager
networkmanager-openvpn
networkmanager-pptp
networkmanager-vpnc
nss-mdns
ntp
mobile-broadband-provider-info
modemmanager
openresolv
openssh
samba
usb_modeswitch

## Connect Packages
android-tools
android-udev
gufw
gvfs
gvfs-afc
gvfs-gphoto2
gvfs-mtp
gvfs-google
gvfs-nfs
gvfs-smb
kamera
mtpfs
udiskie
udisks2

## Xorg Input Drivers
xf86-input-elographics
xf86-input-evdev
xf86-input-libinput
xf86-input-void
xf86-input-wacom

## Xorg Server and Graphics
lib32-libva-intel-driver
lib32-libva-mesa-driver
lib32-libva-vdpau-driver
libva-intel-driver
libva-mesa-driver
libva-vdpau-driver
mesa-demos
lib32-mesa-demos
numlockx
xdg-user-dirs
xorg-server
xorg-twm
xorg-xinit
xorg-xkill

## Libraries for Sound/Audio/Video
alsa-firmware
alsa-utils
ffmpeg
libdvdcss
alsa-lib
lib32-alsa-lib
lib32-alsa-plugins
pulseaudio
pulseaudio-alsa
lib32-libcanberra-pulse
lib32-libpulse
pulseaudio-bluetooth
pulseaudio-ctl
pulseaudio-zeroconf

## GStreamer
grilo-plugins
gst-libav
gst-plugins-bad
gst-plugins-base
gst-plugins-good
gst-plugins-ugly
libcanberra
libcanberra-pulse
libcanberra-gstreamer
phonon-qt5-gstreamer

## Fonts
cantarell-fonts
adobe-source-code-pro-fonts
adobe-source-serif-pro-fonts
adobe-source-sans-pro-fonts
terminus-font
ttf-bitstream-vera
ttf-caladea
ttf-carlito
ttf-dejavu
ttf-font-awesome
ttf-inconsolata
ttf-indic-otf
ttf-droid
ttf-liberation

## Package management
pamac
pamac-tray-appindicator
pamac-cli
apparmor
fwupd

## Java
jdk8-openjdk
jre8-openjdk-headless
jre8-openjdk

## Printing
cups
cups-pk-helper
foomatic-db-engine
ghostscript
gsfonts
gtk3-print-backends
gutenprint
splix
hplip
python-pillow
python-pyqt5
system-config-printer

## AUR Support/Development
# Missing base-devel packages
autoconf
automake
binutils
bison
fakeroot
flex
gcc
gcc-libs-multilib
gcc-multilib
libtool
m4
make
patch
pkg-config
lib32-flex
# Extra packages for AUR support
git
patchutils
subversion
yay

## Display manager
sddm
sddm-kcm

## Plasma5 Group
bluedevil
kde-gtk-config
kdeplasma-addons
kgamma5
kinfocenter
kmenuedit
kscreen
kscreenlocker
ksshaskpass
ksysguard
kwallet-pam
kwayland-integration
kwin
qt5-virtualkeyboard
kwrited
milou
plasma-desktop
plasma-nm
plasma-pa
plasma-simplemenu
plasma-workspace
plasma-workspace-wallpapers
powerdevil
systemsettings
user-manager

## Themes QT/GTK/SDDM
breeze
breeze-gtk
breeze-icons
gnome-icon-theme
gnome-themes-standard
oxygen # needed to provide KDE system sounds
oxygen-icons
manjaro-icons

## KDE Applications
akregator
elisa
ark
dolphin
dolphin-plugins
gwenview
kaffeine
kate
kamoso
kcalc
kcolorchooser
kcm-wacomtablet
kfind
kdeconnect
kmail
kdenetwork-filesharing
kde-servicemenus-rootactions
khelpcenter
kio-extras
kio-gdrive
konsole
konversation
korganizer
kontact
kopete
kolourpaint
kompare
knotes
kmousetool
kmag
krename
krfb
krdc
kgpg
ksystemlog
kruler
kwalletmanager
okular
partitionmanager
spectacle
sweeper
k3b
keditbookmarks
kget
libktorrent
print-manager
skanlite
kcharselect
appmenu-gtk-module
gnome-color-manager

## QT Applications
dmidecode # for inxi -m output
systemd-kcm
powertop
ktorrent
cantata
simplescreenrecorder

## Optional dependencies for dolphin
kdegraphics-thumbnailers  # PDF and PS thumbnails
ffmpegthumbs              # video thumbnails
ruby                      # installing new service menus with GHNS

## Optional dependencies for gwenview
kimageformats     # support for dds, xcf, exr, psd, and more image formats
qt5-imageformats  # support for tiff, webp, and more image formats

## Optional dependencies for k3b
cdparanoia    # for cd ripping support
cdrdao        # for disk-at-once (DAO) mode support
dvd+rw-tools  # for dvd burning support
emovix        # for bootable multimedia cd/dvd support
transcode     # for advanced mpeg conversion support
vcdimager     # for vcd burning support

## Optional dependencies for kaccounts-integration
kaccounts-providers

## Optional dependencies for kdeconnect
sshfs  # remote filesystem browser

## Optional dependencies for okular
poppler-data  # encoding data to display PDF documents containing CJK characters

## Optional dependencies for kdenlive
cdrtools         # for creation of DVD ISO images
dvdauthor        # for creation of DVD
dvgrab           # for firewire capture
recordmydesktop  # for screen capture
movit            # for GPU video processing

## Apps Extra
python-numpy
python-lxml
scour

## Office
libreoffice-fresh

## Browser
firefox
firefox-adblock-plus
firefox-extension-privacybadger
firefox-extension-https-everywhere
firefox-ublock-origin

## Codec
a52dec
faac
faad2
flac
jasper
lame
libdca
libdv
libmad
libmpeg2
libtheora
libvorbis
libxv
wavpack
x264
xvidcore
gstreamer-vaapi

## Compressed
atool
arj
unarj
unace
p7zip
unrar
cpio
zip
unzip
lzop
lrzip
lzip
unarchiver

## Desktop Utils
perl-file-mimeinfo
xdg-utils
kernel-alive

## Misc
manjaro-hotfixes
squashfs-tools

## Manjaro-tools
manjaro-tools-iso
manjaro-iso-profiles-base

# AUR Apps
epson-inkjet-printer-escpr
epson-inkjet-printer-escpr2

## Portal support
xdg-desktop-portal
xdg-desktop-portal-kde

## Developer
# ide
arduino
umbrello
cervisia
kdesvn
kexi
vim
neovim
kdevelop
code
qt5-tools
qtcreator

# lammp
apache
mariadb
php
php-apache
php-cgi
php-gd
php-imap
phpmyadmin

# cli
npm
nodejs
composer
python-pip
yarn
ruby
rubygems
typescript
coffeescript
eslint

# kvm/qemu
qemu
virt-manager
virt-viewer
dnsmasq
vde2
bridge-utils
openbsd-netcat
ebtables
iptables
